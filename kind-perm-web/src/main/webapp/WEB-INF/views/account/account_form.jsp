<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
</head>

<body>
	<div data-options="region: 'center'">
		<div class="container">
			<div class="content">
				<form id="mainform" action="${ctx}/account/account/${action}"
					method="post" novalidate class="form">

					<div class="tabs-panels">
						<div class="panel">
							<div title="" data-options="closable:false"
								class="basic-info panel-body panel-body-noheader panel-body-noborder">
								<div class="column">
									<span class="current">用户基本信息</span>
								</div>
								<table class="kv-table" cellspacing="10">
									<tr>
										<td class="kv-label">姓名：</td>
										<td class="kv-content">
                                            <input type="hidden" name="id" value="${entity.id }" />
											<input name="name" type="text" value="${entity.name}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">电话：</td>
										<td class="kv-content">
											<input name="tel" type="text" value="${entity.tel}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">性别：</td>
										<td class="kv-content">
                                            <select id="sex" name="sex" style="width:140px;" class="easyui-combobox">
                                            	<c:if test="${entity.sex == '0' or entity.sex == '' or entity.sex == null }"><option value="0" selected>--- 请选择 ---</option><option value="1">男</option><option value="2">女</option></c:if>
                                                <c:if test="${entity.sex == '1' }"><option value="0" selected>--- 请选择 ---</option><option value="1" selected>男</option><option value="2">女</option></c:if>
                                            	<c:if test="${entity.sex == '2' }"><option value="0" selected>--- 请选择 ---</option><option value="1">男</option><option value="2" selected>女</option></c:if>
                                            </select>
                                            <%--<input name="sex" type="text" value="${entity.sex}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />--%>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {

			$('#mainform').form({
				onSubmit : function() {
					var isValid = $(this).form('validate');
					// 返回false终止表单提交
					return isValid;
				},
				success : function(data) {
					var dataObj = eval("(" + data + ")");//转换为json对象
					if (submitSuccess(dataObj, dg, d)) {
						dg.treegrid('reload');
					}
				}
			});
		});
	</script>
</body>
</html>