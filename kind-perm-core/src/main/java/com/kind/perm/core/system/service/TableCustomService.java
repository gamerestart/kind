package com.kind.perm.core.system.service;

import java.util.List;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.demo.domain.CommunityDO;
import com.kind.perm.core.system.domain.TableCustomDO;

public interface TableCustomService {


	
	 /**
     * 分页查询
     * @param pageQuery
     * @return
     */
	PageView<TableCustomDO> selectPageList(PageQuery pageQuery);

    /**
     * 保存数据
     * @param entity
     */
	int save(TableCustomDO entity);

    /**
     * 获取数据对象
     * @param id
     * @return
     */
	TableCustomDO getById(Long id);

    /**
     * 删除数据
     * @param id
     */
	void remove(Long id);
	
	/**
	 * 根据类型查询出可导出的自定义属性
	 * @param type
	 * @return
	 */
	public List<TableCustomDO> findTableCustomExport(Long type);
	
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<TableCustomDO> queryList(TableCustomDO entity);
	
	
	
}
