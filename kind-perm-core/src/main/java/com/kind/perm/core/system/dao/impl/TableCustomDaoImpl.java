/**
 * Project Name:kafa-wheat-core
 * File Name:UserRoleDao.java
 * Package Name:com.kind.perm.core.dao
 * Date:2016年6月14日下午5:15:06
 * Copyright (c) 2016, http://www.mcake.com All Rights Reserved.
 *
*/

package com.kind.perm.core.system.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.kind.common.datasource.DataSourceKey;
import com.kind.common.datasource.KindDataSourceHolder;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.system.dao.TableCustomDao;
import com.kind.perm.core.system.domain.TableCustomDO;

/**
 * Function:导出数据持久化接口. <br/>
 * Date: 2017年1月19日 上午10:15:06 <br/>
 * 
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
@Repository
public class TableCustomDaoImpl extends BaseDaoMyBatisImpl<TableCustomDO, Serializable>
implements TableCustomDao{

	@Override
	public List<TableCustomDO> findTableCustomExport(Long type) {
		return super.query(NAMESPACE + "findTableCustomExport", type);
	}

	@Override
	public List<TableCustomDO> page(PageQuery pageQuery) {
		return super.query(NAMESPACE + "page", pageQuery);
	}

	@Override
	public int count(PageQuery pageQuery) {
		return super.count(NAMESPACE + "count", pageQuery);
	}

	@Override
	public int saveOrUpdate(TableCustomDO entity) {
		if (entity.getId() == null) {
			return super.insert(NAMESPACE + "insert", entity);
		} else {
			return super.update(NAMESPACE + "update", entity);
		}
	}

	@Override
	public TableCustomDO getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}

	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
		
	}

	@Override
	public List<TableCustomDO> queryList(TableCustomDO entity) {
		 return super.query(NAMESPACE + "queryList", entity);
	}
	

}
